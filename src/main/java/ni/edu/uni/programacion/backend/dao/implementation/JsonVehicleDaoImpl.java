/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion.backend.dao.implementation;

import com.google.gson.Gson;
import ni.edu.uni.programacion.backend.dao.VehicleDao;
import ni.edu.uni.programacion.backend.pojo.Vehicle;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Implementacion de la interfaz VehicleDao, Esta clase reescribira los metodos de la interfaz para su uso con archivos
 * aleatoreos. Los datos dentro de estos archivos se guardara en formato json.
 *
 * @author yasser.membreno
 */
public class JsonVehicleDaoImpl extends RandomTemplate implements VehicleDao {
    /**
     * SIZE es una constante que se utiliza para la separacion de los json almacenados dentro del archivo data.
     **/
    private final int SIZE = 800;
    private Gson gson;

    public JsonVehicleDaoImpl() throws FileNotFoundException {
        // Se crea la instancia de la clase #RamdomTemplate, creando el par de archivos.
        super(new File("vehicleJson.head"), new File("vehicleJson.dat"));
        gson = new Gson();
    }

    @Override
    public Vehicle findById(int id) throws IOException {
        CustomRandom managerFile = getCustomRandom();
        RandomAccessFile headFile = managerFile.getRafH();

        headFile.seek(0);

        int sizeData = headFile.readInt();
        for (int index = 0; index < sizeData; index++) {
            // 8 es la posicion inicial de donde se guardan los datos.
            // Con esto se obtiene la posicion inicial del head.
            long indexData = 8 + (index * 8);
            headFile.seek(indexData);

            // se obtiene el id del vehiculo
            int idVehicle = headFile.readInt();

            if (id == idVehicle) {
                // se le resta un 1 por que en el metodo create cuando se guarda el json el valor de K aun no esta incrementado.
                // se le suma 4 por que los primeros 4 bytes son el id del json grabado como K.
                long posD = (id - 1) * SIZE + 4;
                getCustomRandom().getRafD().seek(posD);

                return gson.fromJson(getCustomRandom().getRafD().readUTF(), Vehicle.class);
            }
        }

        return null;
    }

    @Override
    public Collection<Vehicle> findByStatus(String status) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void create(Vehicle t) throws IOException {
        // TODO: Los archivos de acceso aleatorio almacenan la informacion como un arreglo de bytes.
        // Las lecturas y escrituras mueven el cursor o puntero del areglo, desde la posicion inicial hasta la cantidad
        // de bytes leidos o escritos. Por ejemplo si tengo un areglo de bytes [0, 1, 2, 3, 4, 5, 6] y realizo un readInt
        // mi posicion en el areglo sera 4, ya que el readInt lee 4 bytes osea [0, 1, 2, 3]


        // Se obtiene el manejador de archivos aleatorio, y se invoca al archivo head, al cual se ubica el puntero en el
        // inicio del archivo.
        getCustomRandom().getRafH().seek(0);

        // Obteniendo del archivo head el ultimo id guardado de head osea el valor de N y ubicando el cursos posterior a ese valor.
        int n = getCustomRandom().getRafH().readInt();

        // Obteniendo del archivo head el ultimo id guardado de data osea el valor de K y ubicando el cursos posterior a ese valor.
        int k = getCustomRandom().getRafH().readInt();

        // Se multiplica el valor del ultimo ID por la constante de tamaño, esto nos da la proxima posicion en donde
        // iniciar a guardar el nuevo json.
        long posD = k * SIZE;

        // se mueve el cursor a la ubicacion de donde se comenzara a guardar el nuevo json
        getCustomRandom().getRafD().seek(posD);//

        // se genera un nuevo id y se escribe
        getCustomRandom().getRafD().writeInt(++k);//guarda el nuevo id

        // luego se guarda el json en la posicion posterior a la escritura del id.
        getCustomRandom().getRafD().writeUTF(gson.toJson(t));//escribe la nueva data

        // se calcula la posicion proxima a escribir en el archivo head.
        long posH = 8 + (n * 8);

        // se mueve el cursor a la posicion en donde se guardara la nueva informacion.
        getCustomRandom().getRafH().seek(posH);

        // se guarda en el archivo head el ultimo id.
        getCustomRandom().getRafH().writeInt(k);

        // se guarda en el archivo head la cantidad en almacen
        getCustomRandom().getRafH().writeInt(t.getStockNumber());

        // se ubica el cursor al inicio del archivo head
        getCustomRandom().getRafH().seek(0);

        // se genera un nuevo id y se guarda en el head
        getCustomRandom().getRafH().writeInt(++n);

        // se guarda el nuevo id de data en el archivo head
        getCustomRandom().getRafH().writeInt(k);

        // importante cerrar los ficheros
        close();

    }

    @Override
    public int update(Vehicle t) throws IOException {
        // TODO No se puede actualizar un registro debido a que el objeto no mantiene el valor del id,
        //  si se quiere solucionar se debera de pasar como parametro o guardar el id en la entidad.
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Vehicle t) throws IOException {
        // TODO No se puede eliminar un registro debido a que el objeto no mantiene el valor del id,
        //  si se quiere solucionar se debera de pasar como parametro o guardar el id en la entidad.
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Collection<Vehicle> getAll() throws IOException {

        // Se crea la lista de objetos en donde se guardaran los json leidos.
        List<Vehicle> vehicles = new ArrayList<>();
        Vehicle vehicle = null;

        // Se mueve el puntero del archivo head a su posicion inicial.
        getCustomRandom().getRafH().seek(0);

        // se lee la cantidad de elementos guardados.
        int n = getCustomRandom().getRafH().readInt();

        // se crea un ciclo de la cantidad leida anteriormente
        for (int i = 0; i < n; i++) {


            long posH = 8 + (i * 8);
            getCustomRandom().getRafH().seek(posH);

            // se obtiene el id
            int id = getCustomRandom().getRafH().readInt();

            if (id <= 0) {
                continue;
            }

            // con el id y el valor maximo se obtiene la posicion del json
            long posD = (id - 1) * SIZE + 4;
            getCustomRandom().getRafD().seek(posD);

            vehicle = gson.fromJson(getCustomRandom().getRafD().readUTF(), Vehicle.class);

            vehicles.add(vehicle);
        }

        return vehicles;
    }

}
