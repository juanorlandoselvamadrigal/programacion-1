/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion.backend.dao.implementation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Clase utilitaria para el manejo de archivos de acceso aleatorio.
 *
 * @author yasser.membreno
 */
public class CustomRandom {

    /**
     * Instancia para el manejo del archivo de acceso aleatorio de cabezera.
     */
    private RandomAccessFile rafH;

    /**
     * Instancia para el manejo del archivo de acceso aleatorio de datos.
     */
    private RandomAccessFile rafD;

    public CustomRandom(File fileHead, File fileData) throws FileNotFoundException, IOException {
        // Los archivos se abren en modo escritura y lectura rw: r-read w-write.
        rafH = new RandomAccessFile(fileHead, "rw");
        rafD = new RandomAccessFile(fileData, "rw");

        if (fileHead.length() == 0) {
            rafH.writeInt(0);
            rafH.writeInt(0);
        }
    }

    public void close() throws IOException {
        if (rafH != null) {
            rafH.close();
        }

        if (rafD != null) {
            rafD.close();
        }

    }

    /**
     * @return devuelve la instancia que manejara el archivo de cabecera en modo de acceso aleatorio.
     */
    public RandomAccessFile getRafH() {
        return rafH;
    }

    /**
     * @return devuelve la instancia que manejara el archivo de datos en modo de acceso aleatorio.
     */
    public RandomAccessFile getRafD() {
        return rafD;
    }
}
