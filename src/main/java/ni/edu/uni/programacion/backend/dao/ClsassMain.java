/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion.backend.dao;

import java.io.FileNotFoundException;
import java.io.IOException;
import ni.edu.uni.programacion.backend.dao.implementation.JsonVehicleDaoImpl;
import ni.edu.uni.programacion.backend.pojo.Vehicle;

/**
 *
 * @author rolando
 */
public class ClsassMain {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        VehicleDao vehicleDao = new JsonVehicleDaoImpl();

        Vehicle vv = new Vehicle(123, 2014, "Nissan", "Sentra", "Sport",
                "VHH45II33", "RED", "BROWN", "120000", 6500f,
                Vehicle.Transmission.MANUAL, "1.8L", null, "Active");


        // vehicleDao.create(vv);
        Vehicle value = vehicleDao.findById(1);
        System.out.println(value);
    }
    
}
